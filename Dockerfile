FROM golang:latest

WORKDIR /app

RUN apt-get update && apt-get install -y curl

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go get -d -v ./...

RUN go build -o payments ./cmd/payments

EXPOSE 8080:8080

CMD ["./payments", "start"]
