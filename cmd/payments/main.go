package main

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"os"
)

func main() {
	if err := New().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
