package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/Cryptle/payments/internal/api"
	"gitlab.com/Cryptle/payments/lib/config"
	"gitlab.com/Cryptle/payments/pkg/db"
	"os"
)

type App struct {
	app *cli.App
}

func New() *App {
	a := &App{
		app: cli.NewApp(),
	}
	cfg := &cli.StringFlag{
		Name:    "config",
		Value:   "config.yaml",
		Aliases: []string{"c"},
		Usage:   "Configuration file.",
	}
	a.app.Usage = "Payments"
	a.app.Flags = []cli.Flag{cfg}

	a.commands()

	return a
}

func (a *App) Run(args []string) error {
	if args == nil {
		args = os.Args
	}
	return a.app.Run(args)
}

func (a *App) Start(c *cli.Context) error {
	config.FileName = c.String("config")
	cfg := config.Get()

	_db, err := db.New(
		cfg.Postgres.Host,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
		cfg.Postgres.Port)
	if err != nil {
		return err
	}

	srv, err := api.NewServer(_db)
	if err != nil {
		return err
	}
	if err = srv.Run(); err != nil {
		return err
	}

	return nil
}

func (a *App) CreateConfig(c *cli.Context) error {
	config.FileName = c.String("config")
	if err := config.WriteDefault(); err != nil {
		return err
	}
	fmt.Printf("created %s\n", config.FileName)
	return nil
}

func (a *App) commands() {
	a.app.Commands = []*cli.Command{
		{
			Name:    "start",
			Aliases: []string{"s"},
			Usage:   "start server",
			Action: func(c *cli.Context) error {
				return a.Start(c)
			},
		},
		{
			Name:    "create-config",
			Aliases: []string{"cc"},
			Usage:   "create config file with default values",
			Action: func(c *cli.Context) error {
				return a.CreateConfig(c)
			},
		},
	}
}
