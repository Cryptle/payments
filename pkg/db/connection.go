package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

func New(host, user, pass, dbName string, port int) (*DB, error) {
	crd := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable",
		host, user, pass, dbName, port)
	db, err := gorm.Open("postgres", crd)
	if err != nil {
		return nil, err
	}
	return &DB{
		db: db,
	}, nil
}

type DB struct {
	db *gorm.DB
}

func (d *DB) DB() *gorm.DB {
	return d.db
}
