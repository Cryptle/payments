package api

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"net/http"
)

type PaymentType string

const (
	AuthorizeTx PaymentType = "authorize"
	ChargeTx    PaymentType = "charge"
	RefundTx    PaymentType = "refund"
	ReverseTx   PaymentType = "reversal"
)

type AmountReq struct {
	Amount float64 `json:"amount" xml:"amount" binding:"required,gt=0"`
}

type PaymentReq struct {
	Type           PaymentType     `json:"type" xml:"type" binding:"required"`
	CustomerEmail  string          `json:"customer_email" xml:"customer_email" binding:"required,email"`
	CustomerPhone  string          `json:"customer_phone" xml:"customer_phone" binding:"required"`
	MerchantUUID   string          `json:"merchant_uuid" xml:"merchant_uuid" binding:"required"`
	ReferencedUUID string          `json:"referenced_uuid" xml:"referenced_uuid"`
	Data           json.RawMessage `json:"data" xml:"data"`
}

type TransactionRes struct {
	Uuid           string  `json:"uuid"`
	Amount         float64 `json:"amount"`
	Status         string  `json:"status"`
	CustomerEmail  string  `json:"customer_email"`
	CustomerPhone  string  `json:"customer_phone"`
	MerchantUUID   string  `json:"merchant_uuid"`
	Type           string  `json:"type"`
	ReferencedUUID string  `json:"referenced_uuid"`
}

func (pq *PaymentReq) Amount() (float64, error) {
	if pq.Type == ReverseTx {
		return 0, nil
	}
	var amountReq AmountReq
	if err := json.Unmarshal(pq.Data, &amountReq); err != nil {
		return 0, err
	}
	return amountReq.Amount, nil
}

func NewPaymentReq(pType PaymentType, amount float64, cEmail, cPhone, MerchID, RefID string) (*PaymentReq, error) {
	var res = &PaymentReq{
		Type:           pType,
		CustomerEmail:  cEmail,
		CustomerPhone:  cPhone,
		MerchantUUID:   MerchID,
		ReferencedUUID: RefID,
	}
	if pType != ReverseTx {
		data, err := json.Marshal(&AmountReq{Amount: amount})
		if err != nil {
			return nil, err
		}
		res.Data = data
	}
	return res, nil
}

type PaymentResp struct {
	TransactionID string      `json:"transaction_id" xml:"transaction-id"`
	Amount        float64     `json:"amount" xml:"amount"`
	Status        string      `json:"status" xml:"status"`
	Type          PaymentType `json:"type" xml:"type"`
	MerchantID    string      `json:"merchant_id" xml:"merchant-id"`
	CustomerEmail string      `json:"customer_email" xml:"customer-email"`
}

func (s *Server) makePayment(c *gin.Context) {
	var (
		amount     float64
		paymentReq PaymentReq
		tx         *models.Transaction
		err        error
	)

	if err = c.ShouldBindJSON(&paymentReq); err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "failed to parse request body")
		return
	}

	amount, err = paymentReq.Amount()
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "failed to parse request body")
		return
	}

	switch paymentReq.Type {
	case AuthorizeTx:
		tx, err = s.txService.AuthorizeTransaction(
			amount, paymentReq.CustomerEmail,
			paymentReq.CustomerPhone, paymentReq.MerchantUUID)
	case ChargeTx:
		tx, err = s.txService.ChargeTransaction(
			amount, paymentReq.CustomerEmail,
			paymentReq.CustomerPhone, paymentReq.MerchantUUID,
			paymentReq.ReferencedUUID)
	case RefundTx:
		tx, err = s.txService.RefundTransaction(
			amount, paymentReq.CustomerEmail,
			paymentReq.CustomerPhone, paymentReq.MerchantUUID,
			paymentReq.ReferencedUUID)
	case ReverseTx:
		tx, err = s.txService.ReverseTransaction(
			paymentReq.CustomerEmail, paymentReq.CustomerPhone,
			paymentReq.MerchantUUID, paymentReq.ReferencedUUID)
	default:
		s.logAndRespondError(c, err, http.StatusBadRequest, "invalid transaction type")
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	} else {
		pres := &PaymentResp{
			TransactionID: tx.UUID,
			Status:        tx.Status.String(),
			Type:          PaymentType(tx.Type.String()),
			MerchantID:    tx.MerchantUUID,
			CustomerEmail: tx.CustomerEmail,
		}
		if paymentReq.Type != ReverseTx {
			pres.Amount = tx.Amount
		}
		c.JSON(http.StatusOK, pres)
	}
}

func (s *Server) getTransactions(c *gin.Context) {
	txDb, err := s.transactionRepo.GetAll()
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "failed to get transactions")
		return
	}
	var txs []*TransactionRes
	for _, tx := range txDb {
		txs = append(txs, &TransactionRes{
			Uuid:           tx.UUID,
			Amount:         tx.Amount,
			CustomerEmail:  tx.CustomerEmail,
			CustomerPhone:  tx.CustomerPhone,
			Status:         tx.Status.String(),
			Type:           tx.Type.String(),
			MerchantUUID:   tx.MerchantUUID,
			ReferencedUUID: tx.ReferencedUUID,
		})
	}

	c.JSON(http.StatusOK, txs)
}
