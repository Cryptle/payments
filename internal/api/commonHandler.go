package api

import (
	"encoding/csv"
	"github.com/gin-gonic/gin"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"io"
	"net/http"
)

func (s *Server) logAndRespondError(c *gin.Context, err error, status int, message string) {
	s.log.Errorf(message+": %v", err)
	c.JSON(status, gin.H{"error": message})
}

func (s *Server) csvImport(c *gin.Context) {
	file, _, err := c.Request.FormFile("file")
	if err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "failed to get file")
		return
	}
	defer file.Close()

	var merchantData []*models.Merchant
	reader := csv.NewReader(file)
	for i := 0; ; i++ {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			s.logAndRespondError(c, err, http.StatusBadRequest, "error reading csv file")
			return
		}
		if len(record) != 5 {
			s.logAndRespondError(c, err, http.StatusBadRequest, "invalid csv file")
			return
		}
		if i > 0 {
			role, err := s.roleRepo.GetOrCreate(record[0])
			if err != nil {
				s.logAndRespondError(c, err, http.StatusInternalServerError, "error creating role")
				return
			}
			merchant := &models.Merchant{
				Name:        record[1],
				Description: record[2],
				Email:       record[3],
				Status:      models.MerchantStatusStrType[record[4]],
				Role:        role,
				RoleID:      role.UUID,
			}
			merchantData = append(merchantData, merchant)
		}
	}

	if err = s.merchantRepo.CreateMerchants(merchantData); err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error creating merchants")
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}
