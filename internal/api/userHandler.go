package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

type RegisterReq struct {
	Name     string `json:"name" xml:"name" binding:"required"`
	Email    string `json:"email" xml:"email" binding:"required,email"`
	Password string `json:"password" xml:"password" binding:"required"`
}

type LoginReq struct {
	Email    string `json:"email" xml:"email" binding:"required,email"`
	Password string `json:"password" xml:"password" binding:"required"`
}

type MerchantData struct {
	Uuid        string `json:"uuid" xml:"uuid" binding:"required"`
	Name        string `json:"name" xml:"name"`
	Description string `json:"description" xml:"description"`
	Email       string `json:"email" xml:"email"`
	Status      string `json:"status" xml:"status"`
	Role        string `json:"role" xml:"role"`
}

func (s *Server) register(c *gin.Context) {
	var (
		register RegisterReq
		role     *models.Role
		err      error
	)

	if err = c.ShouldBindJSON(&register); err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "invalid request")
		return
	}

	role, err = s.roleRepo.GetByName(models.AdminRole)
	if err != nil {
		role = models.NewRole(models.AdminRole)
		if err = s.roleRepo.Create(role); err != nil {
			s.logAndRespondError(c, err, http.StatusInternalServerError, "registration failed")
			return
		}
	}

	var merchant = models.NewMerchant(
		register.Name,
		"",
		register.Email,
		models.Inactive,
		0,
		role)

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(register.Password), bcrypt.DefaultCost)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "registration failed")
		return
	}

	merchant.PasswordHash = hashedPassword
	if err = s.merchantRepo.Create(merchant); err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "registration failed")
		return
	}

	c.JSON(http.StatusCreated, gin.H{"ok": "merchant created"})
}

func (s *Server) login(c *gin.Context) {
	var login LoginReq
	if err := c.ShouldBindJSON(&login); err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "invalid request")
		return
	}

	storedMerchant, err := s.merchantRepo.FindByEmail(login.Email)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "invalid email or password")
		return
	}

	if err = bcrypt.CompareHashAndPassword(storedMerchant.PasswordHash, []byte(login.Password)); err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "invalid email or password")
		return
	}

	token, err := s.authService.GenerateToken(login.Email)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error generating token")
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": token})
}

func (s *Server) displayMerchants(c *gin.Context) {
	merchants, err := s.merchantRepo.GetAll()
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "failed to get all merchants")
		return
	}

	var merchantsRes []*MerchantData
	for _, m := range merchants {
		var role string
		if m.Role != nil {
			role = m.Role.Name
		}
		merchantsRes = append(merchantsRes, &MerchantData{
			Uuid:        m.UUID,
			Name:        m.Name,
			Email:       m.Email,
			Status:      m.Status.String(),
			Description: m.Description,
			Role:        role,
		})
	}

	c.JSON(http.StatusOK, merchantsRes)
}

func (s *Server) updateMerchants(c *gin.Context) {
	var merchant MerchantData
	if err := c.ShouldBindJSON(&merchant); err != nil {
		s.logAndRespondError(c, err, http.StatusBadRequest, "error updating merchant")
		return
	}

	mer, err := s.merchantRepo.FindByID(merchant.Uuid)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error updating merchant")
		return
	}

	mer.Name = merchant.Name
	mer.Email = merchant.Email
	mer.Status = models.MerchantStatusStrType[merchant.Status]
	mer.Description = merchant.Description

	role, err := s.roleRepo.GetByName(merchant.Role)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error updating merchant")
		return
	}
	mer.RoleID = role.UUID
	mer.Role = role

	if err = s.merchantRepo.Update(mer); err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error updating merchant")
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func (s *Server) deleteMerchant(c *gin.Context) {
	mID := c.Param("mId")
	transactions, err := s.transactionRepo.GetTxsByMerchantId(mID)
	if err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error deleting merchant")
		return
	}

	if len(transactions) > 0 {
		s.logAndRespondError(c, err, http.StatusBadRequest,
			"cannot delete merchant with related payment transactions")
		return
	}

	if err = s.merchantRepo.Delete(mID); err != nil {
		s.logAndRespondError(c, err, http.StatusInternalServerError, "error deleting merchant")
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}
