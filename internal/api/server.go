package api

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	dbRepo "gitlab.com/Cryptle/payments/internal/payments/repository/db"
	"gitlab.com/Cryptle/payments/internal/payments/services"
	"gitlab.com/Cryptle/payments/lib/config"
	"gitlab.com/Cryptle/payments/lib/log"
	"gitlab.com/Cryptle/payments/pkg/db"
	"go.uber.org/zap"
	"net/http"
	"strings"
)

type Server struct {
	log             *zap.SugaredLogger
	router          *gin.Engine
	txService       *services.TransactionService
	merchantRepo    *dbRepo.MerchantRepository
	transactionRepo *dbRepo.TransactionRepository
	roleRepo        *dbRepo.RoleRepository
	authService     *services.AuthService
	bgService       *services.BackgroundService
}

func NewServer(db *db.DB) (*Server, error) {
	var (
		r               = gin.Default()
		transactionRepo = dbRepo.NewTransaction(db.DB())
		merchantRepo    = dbRepo.NewMerchant(db.DB())
		txService       = services.NewTransactionService(transactionRepo, merchantRepo)
		srv             = &Server{
			log:             log.Get(),
			router:          r,
			txService:       txService,
			merchantRepo:    merchantRepo,
			transactionRepo: transactionRepo,
			roleRepo:        dbRepo.NewRole(db.DB()),
			authService: services.NewAuthService(
				config.Get().JWT.SecretKey,
				config.Get().JWT.Issuer,
				config.Get().JWT.ExpiresIn,
			),
			bgService: services.NewBackgroundService(transactionRepo),
		}
	)

	// start background jobs
	if err := srv.bgService.StartBackgroundJobs(); err != nil {
		return nil, err
	}

	authorized := r.Group("/")
	r.Use(srv.cors())
	authorized.Use(srv.jwtAuthMiddleware())
	authorized.Use(srv.cors())
	authorized.POST("/payment", srv.makePayment)
	authorized.GET("/merchants", srv.displayMerchants)
	authorized.PUT("/merchants", srv.updateMerchants)
	authorized.DELETE("/merchants/:mId", srv.deleteMerchant)
	authorized.GET("/transactions", srv.getTransactions)
	authorized.POST("/csv-import", srv.csvImport)

	r.POST("/register", srv.register)
	r.POST("/login", srv.login)

	return srv, nil
}

func (s *Server) Run() error {
	return s.router.Run(config.Get().HTTP.ListenHostPort)
}

func (s *Server) cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		for _, loc := range config.Get().HTTP.CORSHosts {
			c.Writer.Header().Set("Access-Control-Allow-Origin", loc)
		}
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "x-auth, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

func (s *Server) jwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			s.log.Warn("authorization header not provided")
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "authorization header not provided"})
			return
		}
		tokenString := strings.Split(authHeader, " ")[1]
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				s.log.Warnf("unexpected signing method: %v", token.Header["alg"])
				return nil, newUnexpectedSigningMethodError(token.Header["alg"])
			}
			return []byte(s.authService.SecretKey), nil
		})
		if err != nil {
			s.log.Warnf("error parsing token: %v", err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			c.Set("email", claims["sub"])
			c.Next()
		} else {
			s.log.Warn("invalid token")
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "invalid token"})
		}
	}
}

func newUnexpectedSigningMethodError(alg any) error {
	return fmt.Errorf("unexpected signing method: %v", alg)
}
