package api

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	repo "gitlab.com/Cryptle/payments/internal/payments/repository/db"
	"gitlab.com/Cryptle/payments/internal/payments/services"
	"gitlab.com/Cryptle/payments/lib/config"
	"gitlab.com/Cryptle/payments/pkg/db"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAuthorizeTransaction(t *testing.T) {
	router, token, merchant := setup(t)
	executeAuthorizeTransaction(t, router, token, merchant)
}

func TestChargeTransaction(t *testing.T) {
	router, token, merchant := setup(t)
	txID := executeAuthorizeTransaction(t, router, token, merchant)
	executeChargeTransaction(t, router, token, txID, merchant)
}

func TestRefundTransaction(t *testing.T) {
	router, token, merchant := setup(t)
	txID := executeAuthorizeTransaction(t, router, token, merchant)
	txID = executeChargeTransaction(t, router, token, txID, merchant)

	body, err := NewPaymentReq(RefundTx, 50.0, "tst@ex.com", "555-555-55", merchant.UUID, txID)
	assert.NoError(t, err)

	jsonBody, err := json.Marshal(body)
	assert.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, "/payment", bytes.NewBuffer(jsonBody))
	assert.NoError(t, err)

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	var paymentResp = &PaymentResp{}
	assert.NoError(t, json.NewDecoder(w.Body).Decode(paymentResp))
	assert.NotNil(t, paymentResp)
	assert.Equal(t, models.Refunded.String(), paymentResp.Status)
	assert.Equal(t, PaymentType(models.Refund.String()), paymentResp.Type)
	assert.Equal(t, "tst@ex.com", paymentResp.CustomerEmail)
	assert.Equal(t, body.MerchantUUID, paymentResp.MerchantID)
	amount, err := body.Amount()
	assert.NoError(t, err)
	assert.Equal(t, amount, paymentResp.Amount)
}

func TestReverseTransaction(t *testing.T) {
	router, token, merchant := setup(t)
	txID := executeAuthorizeTransaction(t, router, token, merchant)

	body, err := NewPaymentReq(ReverseTx, 0, "test@example.com", "555-555-5555", merchant.UUID, txID)
	assert.NoError(t, err)

	jsonBody, err := json.Marshal(body)
	assert.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, "/payment", bytes.NewBuffer(jsonBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	var paymentResp = &PaymentResp{}
	assert.NoError(t, json.NewDecoder(w.Body).Decode(paymentResp))
	assert.NotNil(t, paymentResp)
	assert.Equal(t, models.Reversed.String(), paymentResp.Status)
	assert.Equal(t, PaymentType(models.Reversal.String()), paymentResp.Type)
	assert.Equal(t, "test@example.com", paymentResp.CustomerEmail)
	assert.Equal(t, body.MerchantUUID, paymentResp.MerchantID)
}

func setup(t *testing.T) (*gin.Engine, string, *models.Merchant) {
	gin.SetMode(gin.TestMode)
	config.FileName = "../../config.yaml"
	cfg := config.Get()
	_db, err := db.New(
		cfg.Postgres.Host,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
		cfg.Postgres.Port)
	assert.NoError(t, err)
	s, err := NewServer(_db)
	assert.NoError(t, err)

	var (
		router       = gin.Default()
		merchantRepo = repo.NewMerchant(_db.DB())
		roleRepo     = repo.NewRole(_db.DB())
		role         = models.NewRole(models.AdminRole)
	)

	err = roleRepo.Create(role)
	assert.NoError(t, err)

	merchant := models.NewMerchant(
		"Merchant One",
		"This is Merchant One",
		"mrch@ex.com",
		models.Active, 0, role)
	assert.NoError(t, merchantRepo.Create(merchant))

	auth := services.NewAuthService(cfg.JWT.SecretKey, cfg.JWT.Issuer, cfg.JWT.ExpiresIn)
	token, err := auth.GenerateToken(merchant.Email)
	assert.NoError(t, err)

	router.POST("/payment", s.makePayment)

	return router, token, merchant
}

func executeAuthorizeTransaction(t *testing.T, router *gin.Engine, token string, merchant *models.Merchant) string {
	body, err := NewPaymentReq(AuthorizeTx, 100.0, "tst@ex.com", "555-555-55", merchant.UUID, "")
	assert.NoError(t, err)

	jsonBody, err := json.Marshal(body)
	assert.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, "/payment", bytes.NewBuffer(jsonBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	assert.NoError(t, err)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var paymentResp = &PaymentResp{}
	assert.NoError(t, json.NewDecoder(w.Body).Decode(paymentResp))
	assert.NotNil(t, paymentResp)
	assert.Equal(t, models.Approved.String(), paymentResp.Status)
	assert.Equal(t, PaymentType(models.Authorize.String()), paymentResp.Type)
	assert.Equal(t, "tst@ex.com", paymentResp.CustomerEmail)
	assert.Equal(t, body.MerchantUUID, paymentResp.MerchantID)
	amount, err := body.Amount()
	assert.NoError(t, err)
	assert.Equal(t, amount, paymentResp.Amount)

	return paymentResp.TransactionID
}

func executeChargeTransaction(t *testing.T, router *gin.Engine, token, txID string, merchant *models.Merchant) string {
	body, err := NewPaymentReq(ChargeTx, 50.0, "tst@ex.com", "555-555-55", merchant.UUID, txID)
	assert.NoError(t, err)

	jsonBody, err := json.Marshal(body)
	assert.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, "/payment", bytes.NewBuffer(jsonBody))
	assert.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var paymentResp = &PaymentResp{}
	assert.NoError(t, json.NewDecoder(w.Body).Decode(paymentResp))
	assert.NotNil(t, paymentResp)
	assert.Equal(t, models.Approved.String(), paymentResp.Status)
	assert.Equal(t, PaymentType(models.Charge.String()), paymentResp.Type)
	assert.Equal(t, "tst@ex.com", paymentResp.CustomerEmail)
	assert.Equal(t, body.MerchantUUID, paymentResp.MerchantID)
	amount, err := body.Amount()
	assert.NoError(t, err)
	assert.Equal(t, amount, paymentResp.Amount)

	return paymentResp.TransactionID
}
