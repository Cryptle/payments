package db

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"gitlab.com/Cryptle/payments/pkg/db"
	"testing"
)

func TestMerchantRepository(t *testing.T) {
	_db, err := db.New("localhost", "payment", "payment", "payment", 5442)
	assert.NoError(t, err)

	merchantRepo := NewMerchant(_db.DB())
	roleRepo := NewRole(_db.DB())
	transactionRepo := NewTransaction(_db.DB())

	role := &models.Role{Name: "admin"}
	err = roleRepo.Create(role)
	assert.NoError(t, err)

	merchant := models.NewMerchant(
		"Merchant1",
		"This is Merchant1",
		"merchant1@example.com",
		models.Active,
		0,
		role)

	err = merchantRepo.Create(merchant)
	assert.NoError(t, err)

	merchant, err = merchantRepo.FindByID(merchant.UUID)
	assert.NoError(t, err)
	assert.NotNil(t, merchant)

	// check the role
	assert.Equal(t, role.UUID, merchant.RoleID)
	transaction := models.NewTransaction(
		100.0,
		models.Approved, "", "",
		merchant.UUID,
		models.Authorize,
	)

	err = transactionRepo.Create(transaction)
	assert.NoError(t, err)

	// check the transaction
	merchant, err = merchantRepo.FindByID(merchant.UUID)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(merchant.Transactions))
	assert.Equal(t, transaction.UUID, merchant.Transactions[0].UUID)

	// test Update
	merchant.Status = models.Inactive
	err = merchantRepo.Update(merchant)
	assert.NoError(t, err)

	// check the update
	merchant, err = merchantRepo.FindByID(merchant.UUID)
	assert.NoError(t, err)
	assert.Equal(t, models.Inactive, merchant.Status)

	// test delete
	err = merchantRepo.Delete(merchant.UUID)
	assert.NoError(t, err)

	// check if the merchant is deleted
	merchant, err = merchantRepo.FindByID(merchant.UUID)
	assert.Error(t, err)
}
