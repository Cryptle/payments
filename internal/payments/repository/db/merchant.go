package db

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Cryptle/payments/internal/payments/models"
)

func NewMerchant(db *gorm.DB) *MerchantRepository {
	db.AutoMigrate(&models.Merchant{})
	return &MerchantRepository{DB: db}
}

type MerchantRepository struct {
	DB *gorm.DB
}

func (m *MerchantRepository) GetAll() ([]*models.Merchant, error) {
	var merchantList []*models.Merchant
	if err := m.DB.
		Preload("Transactions").
		Preload("Role").
		Find(&merchantList).Error; err != nil {
		return nil, err
	}
	return merchantList, nil
}

func (m *MerchantRepository) FindByEmail(email string) (*models.Merchant, error) {
	var merchant models.Merchant
	if err := m.DB.
		Preload("Transactions").
		Preload("Role").
		Where("email = ?", email).
		First(&merchant).Error; err != nil {
		return nil, err
	}
	return &merchant, nil
}

func (m *MerchantRepository) FindByID(id string) (*models.Merchant, error) {
	var merchant models.Merchant
	if err := m.DB.
		Preload("Transactions").
		Preload("Role").
		Where("uuid = ?", id).
		First(&merchant).Error; err != nil {
		return nil, err
	}
	return &merchant, nil
}

func (m *MerchantRepository) CreateMerchants(merchants []*models.Merchant) error {
	err := m.DB.Transaction(func(tx *gorm.DB) error {
		// note: bulk insert in not working with this version of gorm
		for _, merchant := range merchants {
			if err := tx.Create(merchant).Error; err != nil {
				return err
			}
		}
		return nil
	})
	return err
}

func (m *MerchantRepository) Create(merchant *models.Merchant) error {
	return m.DB.Create(merchant).Error
}

func (m *MerchantRepository) Update(merchant *models.Merchant) error {
	return m.DB.Save(merchant).Error
}

func (m *MerchantRepository) Delete(uuid string) error {
	if err := m.DB.Where("uuid = ?", uuid).Delete(models.Merchant{}).Error; err != nil {
		return err
	}
	return nil
}
