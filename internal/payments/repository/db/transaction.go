package db

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"time"
)

func NewTransaction(db *gorm.DB) *TransactionRepository {
	db.AutoMigrate(&models.Transaction{})
	return &TransactionRepository{DB: db}
}

type TransactionRepository struct {
	DB *gorm.DB
}

func (r *TransactionRepository) GetAll() ([]*models.Transaction, error) {
	var transactions []*models.Transaction
	if err := r.DB.Find(&transactions).Error; err != nil {
		return nil, err
	}
	return transactions, nil
}

func (r *TransactionRepository) GetTxsByMerchantId(id string) ([]*models.Transaction, error) {
	var transactions []*models.Transaction
	if err := r.DB.Where("merchant_uuid = ?", id).Find(&transactions).Error; err != nil {
		return nil, err
	}
	return transactions, nil
}

func (r *TransactionRepository) FindByID(id string) (*models.Transaction, error) {
	var transaction models.Transaction
	if err := r.DB.Where("uuid = ?", id).First(&transaction).Error; err != nil {
		return nil, err
	}
	return &transaction, nil
}

func (r *TransactionRepository) Create(transaction *models.Transaction) error {
	if err := r.DB.Create(transaction).Error; err != nil {
		return err
	}
	return nil
}

func (r *TransactionRepository) Update(transaction *models.Transaction) error {
	if err := r.DB.Save(transaction).Error; err != nil {
		return err
	}
	return nil
}

func (r *TransactionRepository) Delete(id string) error {
	if err := r.DB.Where("uuid = ?", id).Delete(models.Transaction{}).Error; err != nil {
		return err
	}
	return nil
}

func (r *TransactionRepository) DeleteTransactionsBefore(t time.Time) error {
	return r.DB.Where("created_at < ?", t).Delete(&models.Transaction{}).Error
}
