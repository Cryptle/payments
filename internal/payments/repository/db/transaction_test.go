package db

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"gitlab.com/Cryptle/payments/pkg/db"
	"testing"
)

func TestTransactionRepository(t *testing.T) {
	_db, err := db.New("localhost", "payment", "payment", "payment", 5442)
	assert.NoError(t, err)

	repo := NewTransaction(_db.DB())

	transaction := &models.Transaction{
		Amount: 100.0,
		Status: models.Approved,
	}
	err = repo.Create(transaction)
	assert.NoError(t, err)

	transaction, err = repo.FindByID(transaction.UUID)
	assert.NoError(t, err)
	assert.NotNil(t, transaction)

	transaction.Status = models.Refunded
	transaction.Amount = 50.0
	err = repo.Update(transaction)
	assert.NoError(t, err)

	transaction, err = repo.FindByID(transaction.UUID)
	assert.NoError(t, err)
	assert.NotNil(t, transaction)
	assert.Equal(t, models.Refunded, transaction.Status)
	assert.Equal(t, 50.0, transaction.Amount)

	err = repo.Delete(transaction.UUID)
	assert.NoError(t, err)

	transaction, err = repo.FindByID(transaction.UUID)
	assert.Error(t, err)
}
