package db

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Cryptle/payments/internal/payments/models"
)

func NewRole(db *gorm.DB) *RoleRepository {
	db.AutoMigrate(&models.Role{})
	return &RoleRepository{DB: db}
}

type RoleRepository struct {
	DB *gorm.DB
}

func (r *RoleRepository) GetOrCreate(name string) (*models.Role, error) {
	var role models.Role
	if err := r.DB.Where("name = ?", name).First(&role).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			role = models.Role{Name: name}
			if err := r.DB.Create(&role).Error; err != nil {
				return nil, err
			}
			return &role, nil
		}
		return nil, err
	}
	return &role, nil
}

func (r *RoleRepository) GetByName(name string) (*models.Role, error) {
	var role models.Role
	if err := r.DB.Where("name = ?", name).First(&role).Error; err != nil {
		return nil, err
	}
	return &role, nil
}

func (r *RoleRepository) GetByID(id string) (*models.Role, error) {
	var role models.Role
	if err := r.DB.Where("uuid = ?", id).First(&role).Error; err != nil {
		return nil, err
	}
	return &role, nil
}

func (r *RoleRepository) Create(role *models.Role) error {
	if err := r.DB.Create(role).Error; err != nil {
		return err
	}
	return nil
}

func (r *RoleRepository) Update(role *models.Role) error {
	if err := r.DB.Save(role).Error; err != nil {
		return err
	}
	return nil
}

func (r *RoleRepository) Delete(id string) error {
	if err := r.DB.Where("uuid = ?", id).Delete(models.Role{}).Error; err != nil {
		return err
	}
	return nil
}
