package db

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"gitlab.com/Cryptle/payments/pkg/db"
	"testing"
)

func TestRoleRepository(t *testing.T) {
	_db, err := db.New("localhost", "payment", "payment", "payment", 5442)
	assert.NoError(t, err)

	repo := NewRole(_db.DB())

	role := &models.Role{
		Name: "admin",
	}
	err = repo.Create(role)
	assert.NoError(t, err)

	role, err = repo.GetByID(role.UUID)
	assert.NoError(t, err)
	assert.NotNil(t, role)

	role.Name = "merchant"
	err = repo.Update(role)
	assert.NoError(t, err)

	role, err = repo.GetByID(role.UUID)
	assert.NoError(t, err)
	assert.NotNil(t, role)
	assert.Equal(t, "merchant", role.Name)

	err = repo.Delete(role.UUID)
	assert.NoError(t, err)

	role, err = repo.GetByID(role.UUID)
	assert.Error(t, err)
}
