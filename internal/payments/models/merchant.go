package models

type MerchantStatusType int

const (
	Active MerchantStatusType = iota + 2000
	Inactive
)

var MerchantStatusTypeStr = map[MerchantStatusType]string{
	Active:   "active",
	Inactive: "inactive",
}

var MerchantStatusStrType = map[string]MerchantStatusType{
	"active":   Active,
	"inactive": Inactive,
}

func (ms MerchantStatusType) String() string {
	return MerchantStatusTypeStr[ms]
}

type Merchant struct {
	BaseModel
	PasswordHash        []byte
	Name                string
	Description         string
	Email               string
	Status              MerchantStatusType
	TotalTransactionSum float64
	Transactions        []*Transaction `gorm:"foreignkey:MerchantUUID"`
	Role                *Role          `gorm:"foreignKey:RoleID"`
	RoleID              string
}

func NewMerchant(name, description, email string,
	status MerchantStatusType, totalTransactionSum float64, role *Role) *Merchant {

	return &Merchant{
		Name:                name,
		Description:         description,
		Email:               email,
		Status:              status,
		TotalTransactionSum: totalTransactionSum,
		Role:                role,
		RoleID:              role.UUID,
	}
}
