package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type BaseModel struct {
	UUID      string `gorm:"type:uuid;primary_key;default:uuid_generate_v4()"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (base *BaseModel) BeforeCreate(*gorm.DB) (err error) {
	return
}

func (base *BaseModel) BeforeUpdate(*gorm.DB) (err error) {
	return
}
