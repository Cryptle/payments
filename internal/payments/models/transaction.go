package models

type TxStatusType int
type TxType int

const (
	Approved TxStatusType = iota + 1000
	Reversed
	Refunded
	Error
)

const (
	Authorize TxType = iota + 3000
	Charge
	Refund
	Reversal
)

var TxTypeStr = map[TxType]string{
	Authorize: "authorize",
	Charge:    "charge",
	Refund:    "refund",
	Reversal:  "reversal",
}

var StatusTypeStr = map[TxStatusType]string{
	Approved: "approved",
	Reversed: "reversed",
	Refunded: "refunded",
	Error:    "error",
}

func (rt TxStatusType) String() string {
	return StatusTypeStr[rt]
}

func (tt TxType) String() string {
	return TxTypeStr[tt]
}

type Transaction struct {
	BaseModel
	Amount         float64
	Status         TxStatusType
	CustomerEmail  string
	CustomerPhone  string
	MerchantUUID   string
	Type           TxType
	ReferencedUUID string
}

func NewTransaction(amount float64, status TxStatusType, customerEmail string,
	customerPhone string, merchantUUID string, txType TxType) *Transaction {
	return &Transaction{
		BaseModel:     BaseModel{},
		Amount:        amount,
		Status:        status,
		CustomerEmail: customerEmail,
		CustomerPhone: customerPhone,
		MerchantUUID:  merchantUUID,
		Type:          txType,
	}
}
