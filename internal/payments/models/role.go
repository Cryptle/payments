package models

const (
	AdminRole    = "admin"
	MerchantRole = "merchant"
)

type Role struct {
	BaseModel
	Name      string
	Merchants []*Merchant `gorm:"foreignKey:RoleID"`
}

func NewRole(name string) *Role {
	return &Role{
		Name: name,
	}
}
