package services

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

func NewAuthService(secretKey, issuer string, expiry time.Duration) *AuthService {
	return &AuthService{
		SecretKey: secretKey,
		Issuer:    issuer,
		Expiry:    expiry,
	}
}

type AuthService struct {
	SecretKey string
	Issuer    string
	Expiry    time.Duration
}

func (s *AuthService) GenerateToken(email string) (string, error) {
	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(s.Expiry).Unix(),
		Issuer:    s.Issuer,
		Subject:   email,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(s.SecretKey))
	if err != nil {
		return "", err
	}

	return ss, nil
}
