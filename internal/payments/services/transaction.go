package services

import (
	"errors"
	"fmt"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	"gitlab.com/Cryptle/payments/internal/payments/repository/db"
)

var (
	ErrRefundUnapprovedCharge     = errors.New("only approved Charge transactions can be refunded")
	ErrSubmitInactiveMerchant     = errors.New("transaction cannot be submitted unless the merchant is in active state")
	ErrInsufficientMerchantFunds  = errors.New("the merchant does not have sufficient funds")
	ErrReverseUnapprovedAuthorize = errors.New("only approved Authorize transactions can be reversed")
	ErrMerchantNotActive          = errors.New("merchant is not active")
)

func NewInactiveMerchantError(merchantUUID string) error {
	return fmt.Errorf("merchant with uuid %s is not active", merchantUUID)
}

func NewUnapprovedTransactionError(referencedUUID string) error {
	return fmt.Errorf("transaction with uuid %s is not approved", referencedUUID)
}

type TransactionService struct {
	transactionRepo *db.TransactionRepository
	merchantRepo    *db.MerchantRepository
}

func NewTransactionService(
	transactionRepo *db.TransactionRepository,
	merchantRepo *db.MerchantRepository) *TransactionService {

	return &TransactionService{
		transactionRepo: transactionRepo,
		merchantRepo:    merchantRepo,
	}
}

// AuthorizeTransaction authorizes a transaction
func (s *TransactionService) AuthorizeTransaction(
	amount float64, customerEmail string, customerPhone string,
	merchantUUID string) (*models.Transaction, error) {

	merchant, err := s.merchantRepo.FindByID(merchantUUID)
	if err != nil {
		return nil, err
	}
	if merchant.Status != models.Active {
		return nil, ErrMerchantNotActive
	}

	transaction := models.NewTransaction(
		amount, models.Approved,
		customerEmail, customerPhone,
		merchantUUID, models.Authorize,
	)

	if err = s.transactionRepo.Create(transaction); err != nil {
		return nil, err
	}

	return transaction, nil
}

// ChargeTransaction charges an Authorize transaction
func (s *TransactionService) ChargeTransaction(
	amount float64, cstEmail, cstPhone, mUUID, refUUID string) (*models.Transaction, error) {

	merchant, err := s.merchantRepo.FindByID(mUUID)
	if err != nil {
		return nil, err
	}

	if merchant.Status != models.Active {
		return nil, NewInactiveMerchantError(mUUID)
	}

	// check if the referenced transaction is approved
	referencedTransaction, err := s.transactionRepo.FindByID(refUUID)
	if err != nil {
		return nil, err
	}

	if referencedTransaction.Status != models.Approved {
		return nil, NewUnapprovedTransactionError(refUUID)
	}

	// create the Charge transaction
	transaction := models.NewTransaction(amount, models.Approved, cstEmail, cstPhone, mUUID, models.Charge)
	transaction.ReferencedUUID = refUUID

	if err = s.transactionRepo.Create(transaction); err != nil {
		return nil, err
	}

	// update the merchant's total transaction sum
	merchant.TotalTransactionSum += transaction.Amount
	err = s.merchantRepo.Update(merchant)
	if err != nil {
		return nil, err
	}

	return transaction, nil
}

// RefundTransaction refunds a Charge transaction
func (s *TransactionService) RefundTransaction(
	amount float64, cstEmail, cstPhone, mUUID, refUUID string) (*models.Transaction, error) {

	refTx, err := s.transactionRepo.FindByID(refUUID)
	if err != nil {
		return nil, err
	}

	if refTx.Status != models.Approved || refTx.Type != models.Charge {
		return nil, ErrRefundUnapprovedCharge
	}

	merchant, err := s.merchantRepo.FindByID(mUUID)
	if err != nil {
		return nil, err
	}

	if merchant.Status != models.Active {
		return nil, ErrSubmitInactiveMerchant
	}

	if merchant.TotalTransactionSum < amount {
		return nil, ErrInsufficientMerchantFunds
	}

	tx := models.NewTransaction(
		amount, models.Refunded, cstEmail, cstPhone, mUUID, models.Refund)

	tx.ReferencedUUID = refUUID

	if err = s.transactionRepo.Create(tx); err != nil {
		return nil, err
	}

	merchant.TotalTransactionSum -= amount
	if err = s.merchantRepo.Update(merchant); err != nil {
		return nil, err
	}

	refTx.Status = models.Refunded
	if err = s.transactionRepo.Update(refTx); err != nil {
		return nil, err
	}

	return tx, nil
}

// ReverseTransaction reverses an approved Authorize transaction
func (s *TransactionService) ReverseTransaction(
	customerEmail, customerPhone, merchantUUID,
	referencedUUID string) (*models.Transaction, error) {

	refTx, err := s.transactionRepo.FindByID(referencedUUID)
	if err != nil {
		return nil, err
	}

	if refTx.Status != models.Approved || refTx.Type != models.Authorize {
		return nil, ErrReverseUnapprovedAuthorize
	}

	merchant, err := s.merchantRepo.FindByID(merchantUUID)
	if err != nil {
		return nil, err
	}

	if merchant.Status != models.Active {
		return nil, ErrSubmitInactiveMerchant
	}

	// reverse transactions do not have amount
	tx := models.NewTransaction(0, models.Reversed, customerEmail, customerPhone, merchantUUID, models.Reversal)
	tx.ReferencedUUID = referencedUUID

	if err = s.transactionRepo.Create(tx); err != nil {
		return nil, err
	}

	refTx.Status = models.Reversed
	if err = s.transactionRepo.Update(refTx); err != nil {
		return nil, err
	}

	return tx, nil
}
