package services

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/payments/internal/payments/models"
	dbRepo "gitlab.com/Cryptle/payments/internal/payments/repository/db"
	"gitlab.com/Cryptle/payments/pkg/db"
	"testing"
)

func setupDB(t *testing.T) (*gorm.DB, string) {
	_db, err := db.New("localhost", "payment", "payment", "payment", 5442)
	assert.NoError(t, err)
	_db.DB().AutoMigrate(&models.Transaction{})

	roleRepo := dbRepo.NewRole(_db.DB())
	role := models.NewRole(models.AdminRole)
	err = roleRepo.Create(role)
	assert.NoError(t, err)

	merchant := models.NewMerchant(
		"Merchant1",
		"This is Merchant1",
		"merchant1@example.com",
		models.Active, 0, role)
	_db.DB().Save(&merchant)

	return _db.DB(), merchant.UUID
}

func TestAuthorizeTransaction(t *testing.T) {
	var (
		_db, mUUID         = setupDB(t)
		transactionRepo    = dbRepo.NewTransaction(_db)
		merchantRepo       = dbRepo.NewMerchant(_db)
		transactionService = NewTransactionService(transactionRepo, merchantRepo)
	)

	tx, err := transactionService.AuthorizeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID)

	assert.NoError(t, err)
	assert.Equal(t, models.Authorize, tx.Type)
	assert.Equal(t, models.Approved, tx.Status)
	assert.Equal(t, 1000.0, tx.Amount)
}

func TestChargeTransaction(t *testing.T) {
	var (
		_db, mUUID         = setupDB(t)
		transactionRepo    = dbRepo.NewTransaction(_db)
		merchantRepo       = dbRepo.NewMerchant(_db)
		transactionService = NewTransactionService(transactionRepo, merchantRepo)
	)

	// authorize transaction before charging
	authTx, err := transactionService.AuthorizeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID)
	assert.NoError(t, err)

	tx, err := transactionService.ChargeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID, authTx.UUID)

	assert.NoError(t, err)
	assert.Equal(t, models.Charge, tx.Type)
	assert.Equal(t, models.Approved, tx.Status)
}

func TestRefundTransaction(t *testing.T) {
	var (
		_db, mUUID         = setupDB(t)
		transactionRepo    = dbRepo.NewTransaction(_db)
		merchantRepo       = dbRepo.NewMerchant(_db)
		transactionService = NewTransactionService(transactionRepo, merchantRepo)
	)

	// authorize and charge transaction before refunding
	authTx, err := transactionService.AuthorizeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID)
	assert.NoError(t, err)

	// charge transaction after authorizing
	chargeTx, err := transactionService.ChargeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID, authTx.UUID)
	assert.NoError(t, err)

	// refund transaction after charging
	tx, err := transactionService.RefundTransaction(
		500.0, "test@example.com",
		"555-555-5555", mUUID, chargeTx.UUID)

	assert.NoError(t, err)
	assert.Equal(t, models.Refund, tx.Type)
	assert.Equal(t, models.Refunded, tx.Status)
	assert.Equal(t, 500.0, tx.Amount)
}

func TestReverseTransaction(t *testing.T) {
	var (
		_db, mUUID         = setupDB(t)
		transactionRepo    = dbRepo.NewTransaction(_db)
		merchantRepo       = dbRepo.NewMerchant(_db)
		transactionService = NewTransactionService(transactionRepo, merchantRepo)
	)

	// authorize transaction before reversing
	authTx, err := transactionService.AuthorizeTransaction(
		1000.0, "test@example.com",
		"555-555-5555", mUUID)
	assert.NoError(t, err)

	// reverse transaction after authorizing
	tx, err := transactionService.ReverseTransaction(
		"test@example.com", "555-555-5555",
		mUUID, authTx.UUID)

	assert.NoError(t, err)
	assert.Equal(t, models.Reversal, tx.Type)
	assert.Equal(t, models.Reversed, tx.Status)
	assert.Equal(t, 0.0, tx.Amount)
}
