package services

import (
	"github.com/robfig/cron/v3"
	dbRepo "gitlab.com/Cryptle/payments/internal/payments/repository/db"
	"gitlab.com/Cryptle/payments/lib/config"
	"gitlab.com/Cryptle/payments/lib/log"
	"go.uber.org/zap"
	"time"
)

type BackgroundService struct {
	txRepo *dbRepo.TransactionRepository
	log    *zap.SugaredLogger
}

func NewBackgroundService(repo *dbRepo.TransactionRepository) *BackgroundService {
	return &BackgroundService{
		txRepo: repo,
		log:    log.Get(),
	}
}

// StartBackgroundJobs starts background jobs
func (s *BackgroundService) StartBackgroundJobs() error {
	c := cron.New(cron.WithSeconds())
	if _, err := c.AddFunc(config.Get().BackgroundService.CronSpec, s.deleteOldTransactions); err != nil {
		return err
	}
	c.Start()
	return nil
}

// deleteOldTransactions deletes transactions older than the duration specified in the config
func (s *BackgroundService) deleteOldTransactions() {
	var ago = time.Now().Add(config.Get().BackgroundService.DeleteTransactionsDuration)
	if err := s.txRepo.DeleteTransactionsBefore(ago); err != nil {
		s.log.Error("failed to delete old transactions: %v\n", err)
	} else {
		s.log.Info("old transactions deleted successfully")
	}
}
