package config

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"sync"
	"time"
)

type Config struct {
	Log struct {
		Format string `yaml:"format"`
		Level  string `yaml:"level"`
		File   string `yaml:"file"`
	} `yaml:"log"`
	Postgres struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		Database string `yaml:"database"`
		Schema   string `yaml:"schema"`
	} `yaml:"postgres"`
	HTTP struct {
		ListenHostPort    string        `yaml:"listen_host_port"`
		URLBase           string        `yaml:"url_base"`
		RegisterWhiteList []string      `yaml:"register_whitelist"`
		RequestTimeout    time.Duration `yaml:"request_timeout"`
		CORSHosts         []string      `yaml:"cors_hosts"`
		ServeLocalFiles   bool          `yaml:"serve_local_files"`
	} `yaml:"http"`
	JWT struct {
		Issuer    string        `yaml:"issuer"`
		ExpiresIn time.Duration `yaml:"expires_in"`
		SecretKey string        `yaml:"secret-key"`
	} `yaml:"jwt"`
	BackgroundService struct {
		CronSpec                   string        `yaml:"cron_spec"`
		DeleteTransactionsDuration time.Duration `yaml:"delete_transactions_duration"`
	} `yaml:"background_service"`
}

var FileName string
var instance *Config
var once sync.Once

func GetDefault() *Config {
	cfg := &Config{}
	cfg.Postgres.Host = "localhost"
	cfg.Postgres.Port = 5442
	cfg.Postgres.User = "payment"
	cfg.Postgres.Password = ""
	cfg.Postgres.Database = "payment"
	cfg.Postgres.Schema = "payment"
	cfg.Log.Format = "text"
	cfg.Log.Level = "debug"
	cfg.Log.File = ""
	cfg.HTTP.ListenHostPort = "127.0.0.1:8080"
	cfg.HTTP.URLBase = "http://localhost"
	cfg.HTTP.RegisterWhiteList = []string{"127.0.0.1"}
	cfg.HTTP.CORSHosts = []string{"http://localhost:8080", "*"}
	cfg.HTTP.ServeLocalFiles = true
	cfg.JWT.ExpiresIn = 2 * 24 * 60 * 60 * time.Second // 48 hours
	cfg.JWT.Issuer = "payments"
	cfg.JWT.SecretKey = "___secret___"
	cfg.BackgroundService.CronSpec = "0 * * * * *"                    // every minute
	cfg.BackgroundService.DeleteTransactionsDuration = -1 * time.Hour //older than 1 hour
	return cfg
}

func Get() *Config {
	once.Do(func() {
		instance = GetDefault()
		data, err := os.ReadFile(FileName)
		if err != nil {
			fmt.Printf("failed to open %s: %s\ncontinuing with defaults", FileName, err.Error())
		} else {
			if err = yaml.Unmarshal(data, instance); err != nil {
				fmt.Printf("failed to ungob config file: %s", err.Error())
			}
		}
	})
	return instance
}

func WriteDefault() error {
	cfg := GetDefault()
	_, err := os.Stat(FileName)
	if os.IsNotExist(err) {
		data, err := yaml.Marshal(cfg)
		if err != nil {
			return err
		}
		if err := os.WriteFile(FileName, data, 0664); err != nil {
			return err
		}
		return nil
	}
	return errors.New("file exists")
}
