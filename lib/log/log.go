package log

import (
	"gitlab.com/Cryptle/payments/lib/config"
	"go.uber.org/zap"
	"log"
	"os"
	"path"
	"strings"
	"sync"
)

func NewDevLogger(level, file string) *zap.SugaredLogger {
	cfg := zap.NewDevelopmentConfig()
	cfg.Level = toZapLevel(level)
	cfg.DisableStacktrace = true

	if file != "" {
		logPath, err := os.Getwd()
		if err != nil {
			log.Println(err)
		}
		cfg.OutputPaths = []string{
			path.Join(logPath, file),
		}
	}
	l, _ := cfg.Build()

	return l.Sugar()
}

func NewProdLogger(level, file string) *zap.SugaredLogger {

	cfg := zap.NewProductionConfig()
	cfg.Level = toZapLevel(level)
	cfg.DisableStacktrace = true
	if file != "" {
		logPath, err := os.Getwd()
		if err != nil {
			log.Println(err)
		}
		cfg.OutputPaths = []string{
			path.Join(logPath, file),
		}
	}
	l, _ := cfg.Build()

	return l.Sugar()
}

var instance *zap.SugaredLogger
var once sync.Once

func Get() *zap.SugaredLogger {
	once.Do(func() {
		cfg := config.Get()
		switch cfg.Log.Format {
		case "text":
			instance = NewDevLogger(cfg.Log.Level, cfg.Log.File)
		default:
			instance = NewProdLogger("info", cfg.Log.File)
		}
	})

	return instance
}

func toZapLevel(level string) zap.AtomicLevel {

	switch strings.ToLower(level) {
	case "debug":
		return zap.NewAtomicLevelAt(zap.DebugLevel)
	case "info":
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	case "warning":
		return zap.NewAtomicLevelAt(zap.WarnLevel)
	case "error":
		return zap.NewAtomicLevelAt(zap.ErrorLevel)
	case "panic":
		return zap.NewAtomicLevelAt(zap.PanicLevel)
	case "fatal":
		return zap.NewAtomicLevelAt(zap.FatalLevel)
	default:
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	}
}
