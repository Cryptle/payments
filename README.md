# Payments

Payments is a transaction system that enables merchants to handle payment transactions. The application provides
merchants with the ability to charge and refund payments, and monitor transaction statuses. It also includes a CSV
import functionality.

## Getting Started / Installation

To get a local copy up and running:

* Clone the repository with `git clone https://gitlab.com/Cryptle/payments.git`
* Start a PostgreSQL database server using `docker-compose up -d` or install PostgreSQL locally
* Navigate into the project directory with `cd payments`
* `make build` to build the project `make run` to run the project
* For running tests, use `make tests` 
* For generate config-file use `./payments cc` this will create a config file in the home directory
  * Edit the config file with the required values.
* Start the server with `./payments s`

Build the client UI:
* Install the required dependencies for the client by navigating to the client UI directory (`cd payments-ui`) and
   running `npm install` and `npm start` to start the client UI
* Navigate to `http://localhost:3000` to view the application

## Usage

To use the application, a user needs to register and then log in. Once logged in, the user can navigate between the "
Merchants", "Transactions" and "CSVImport" pages using the navigation bar at the top of the page.

* On the "Merchants" page, the user can view a list of all merchants.
* On the "Transactions" page, the user can view all transactions, and have the ability to charge, refund, and reverse
transactions.
* On the "CSVImport" page, the user can import merchant data from a CSV file.

The user can log out of the application using the "Logout" link in the navigation bar.

## Features

- Conduct different types of transactions (authorize, charge, refund, reverse).
- User registration and login functionality.
- Handle merchants (display, update, delete).
- Display transactions.
- Create, edit, delete and list merchant data.
- Roles and permissions (admin, merchant).
- CSV import for bulk data entries.
- Uses a cron-job logic to remove old transactions.
- REST API endpoints.
- GWT token-based authentication.
- Uses a PostgreSQL database.
- React UI for all endpoints.

## CSV Import

Payments application also provides a way to bulk import merchant data using CSV files. The CSV file format should be as follows:

Example CSV file format:
```csv
role,name,description,email,status
admin,Admin 1,This is Admin 1,admin1@example.com,active
merchant,Merchant 2,This is Merchant 2,merchant2@example.com,inactive
admin,Admin 2,This is Admin 2,admin2@example.com,active
```

This will create three users with their respective roles, names, descriptions, emails, and statuses.

### Cron-Job Logic
The Payments application uses a cron-job logic to automatically remove old transactions.

## API Endpoints

Here are the key API endpoints the Payments application exposes:

**POST /register**

This endpoint allows a user to register with the system.

Request:

```json
{
   "name": "some-name",
   "email": "some@mail.com",
   "password": "some-password"
}
```

Response:

```json
{
  "token": "ok | error"
}
```

**POST /login**

This endpoint allows a user to login to the system.

Request:

```json
{
   "email": "some@mail.com",
   "password": "some-password"
}
```  

Response:

```json
{
  "token": "the-auth-token"
}
```

**POST /payment**

This endpoint handles payment transactions, which could be an authorization, a charge, a refund, or a
reversal.

Request:

```json
{
  "type": "authorize",
  "customer_email": "customer@example.com",
  "customer_phone": "123456789",
  "merchant_uuid": "merchant-uuid",
  "referenced_uuid": "referenced-uuid",
  "data": {
    "amount": 100
  }
}
```

Response:

```json
{
  "transaction_id": "transaction-uuid",
  "amount": 100,
  "status": "successful",
  "type": "authorize",
  "merchant_id": "merchant-uuid",
  "customer_email": "customer@example.com"
}
```

**GET /merchants**

This endpoint retrieves a list of all merchants.

Response:

```json
[
  {
    "uuid": "merchant-uuid",
    "name": "merchant-name",
    "description": "merchant-description",
    "email": "merchant-email",
    "status": "merchant-status",
    "role": "admin"
  }
]
```

**PUT /merchants**: This endpoint updates merchant data.

Request:

```json
{
  "name": "merchant-name",
  "description": "merchant-description",
  "status": "merchant-status",
  "role": "admin"
}
```

Response:

```json
{
  "status": "ok | error"
}
```

**DELETE /merchants/:mId**: This endpoint deletes a specific merchant given a merchant ID.

Response:

```json
{
  "status": "ok | error"
}
```

**GET /transactions**: This endpoint retrieves all transactions.

Response:

```json
[
  {
    "uuid": "transaction-uuid",
    "amount": 100,
    "status": "approved | reversed | refunded | error",
    "customer_email": "some@email.com",
    "customer_phone": "123456789",
    "merchant_uuid": "merchant-uuid",
    "type": "authorize | charge | refund | reversal",
    "referenced_uuid": "referenced-uuid"
  }
]
```

**POST /csv-import**

This endpoint allows for the import of transaction data from a CSV file.

Request:

```json
{
  "file": "file"
}
```

Response:

```json
{
  "status": "ok | error"
}
```

# Docker

### Build Docker Images

For building backend docker image, 
run the following command in the root project directory:

``` bash
docker build -t payments:0.0.1 -f Dockerfile .
```

For building frontend docker image, run the following commands:

From the root project directory:
``` bash
cd payments-ui
```

Create a docker image for the frontend:
``` bash
docker build -t payments-ui:0.0.1 -f Dockerfile .
``` 

### Run Docker Images using docker-compose

From the root project directory, run the following command:

``` bash
docker-compose up
```


## Screenshots

![Image description](./assets/p6.png)

![Image description](./assets/p2.png)

![Image description](./assets/p1.png)

![Image description](./assets/p3.png)

![Image description](./assets/p4.png)

![Image description](./assets/p5.png)
