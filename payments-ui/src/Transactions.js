import React, {useEffect, useState} from "react";
import axios from "axios";
import {DataGrid} from "@mui/x-data-grid";
import {apiUrl, AuthContext} from "./App";

const Transactions = () => {
    const [transactions, setTransactions] = useState([]);
    const {setLoggedIn} = React.useContext(AuthContext);
    useEffect(() => {
        const fetchTransactions = async () => {
            const token = localStorage.getItem('token');
            const response = await axios.get(apiUrl + "/transactions", {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            setLoggedIn(true);
            if (response && response.data) {
                setTransactions(response.data);
            } else {
                setTransactions([]);
            }
        };
        fetchTransactions();
    }, []);

    const columns = [
        {field: 'uuid', headerName: 'ID', width: 330},
        {field: 'amount', headerName: 'Amount', width: 90},
        {field: 'status', headerName: 'Status', width: 90},
        {field: 'customer_email', headerName: 'Customer email', width: 130},
        {field: 'customer_phone', headerName: 'Customer phone', width: 130},
        {field: 'merchant_uuid', headerName: 'Merchant id', width: 330},
        {field: 'type', headerName: 'Type', width: 80},
        {field: 'referenced_uuid', headerName: 'Referenced id', width: 330},
    ];

    return (
        <div style={{height: 600, width: '100%'}}>
            <DataGrid rows={transactions} columns={columns} pageSize={5} getRowId={(row) => row.uuid}/>
        </div>
    );
};

export default Transactions;