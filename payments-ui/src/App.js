import React, {useContext, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes, useNavigate} from 'react-router-dom';
import Login from './Login';
import Register from './Register';
import {SnackbarProvider} from "notistack";
import PrivateRoute from "./PrivateRoute";
import Merchants from "./Merchants";
import Transactions from "./Transactions";
import CSVImport from "./CSVImport";
import {AppBar, Toolbar} from "@mui/material";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

export const AuthContext = React.createContext({
    loggedIn: false, setLoggedIn: () => {}
});

export const apiUrl = window.REACT_APP_API_URL || 'http://localhost:8080';

function Navigation() {
    const {loggedIn, setLoggedIn} = useContext(AuthContext);
    const navigate = useNavigate();

    const handleLogout = () => {
        setLoggedIn(false);
        localStorage.removeItem('token');
        navigate("/login");
    }

    const navTransactions = () => {
        navigate("/transactions");
    }
    const navMerchants = () => {
        navigate("/merchants");
    }
    const navLogin = () => {
        navigate("/login");
    }
    const navRegister = () => {
        navigate("/register");
    }
    const navCsvImport = () => {
        navigate("/csv-import");
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h5">
                    Payments
                </Typography>
                {loggedIn ? (
                    <>
                        <Button color="inherit" onClick={navMerchants}>
                            Merchants
                        </Button>
                        <Button color="inherit" onClick={navCsvImport}>
                            CSV Import
                        </Button>
                        <Button color="inherit" onClick={navTransactions}>
                            Transactions
                        </Button>
                        <Button color="inherit" onClick={handleLogout}>
                            Logout
                        </Button>
                    </>
                ) : (
                    <>
                        <Button color="inherit" onClick={navLogin}>
                            Login
                        </Button>
                        <Button color="inherit" onClick={navRegister}>
                            Register
                        </Button>
                    </>
                )}
            </Toolbar>
        </AppBar>
    );
}

function App() {

    return (
        <SnackbarProvider maxSnack={3}>
            <Router>
                <div>
                    <Navigation/>

                    <Routes>
                        <Route path="/login" element={<Login/>}/>
                        <Route path="/register" element={<Register/>}/>
                        <Route path="/merchants" element={(
                            <PrivateRoute><Merchants/> </PrivateRoute>
                        )}/>
                        <Route path="/transactions" element={(
                            <PrivateRoute><Transactions/> </PrivateRoute>
                        )}/>
                        <Route path="/csv-import" element={(
                            <PrivateRoute><CSVImport/> </PrivateRoute>
                        )}/>

                    </Routes>
                </div>
            </Router>
        </SnackbarProvider>
    );
}

export default App;