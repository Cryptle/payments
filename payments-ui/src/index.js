import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App, { AuthContext } from './App';
import reportWebVitals from './reportWebVitals';

function Main() {
    const [loggedIn, setLoggedIn] = useState(false);

    return (
        <AuthContext.Provider value={{ loggedIn, setLoggedIn }}>
            <App />
        </AuthContext.Provider>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        <Main />
    </React.StrictMode>
);

reportWebVitals();