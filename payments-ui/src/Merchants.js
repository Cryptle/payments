import React, {useEffect, useState} from "react";
import axios from "axios";
import {DataGrid} from "@mui/x-data-grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    InputLabel, MenuItem, Select
} from "@mui/material";
import {enqueueSnackbar} from "notistack";
import {apiUrl, AuthContext} from "./App";


const Merchants = () => {

    const [merchants, setMerchants] = useState([]);
    const [editData, setEditData] = useState({name: '', description: ''});
    const [openDialog, setOpenDialog] = useState(false);
    const [refresh, setRefresh] = useState(false);

    const { setLoggedIn } = React.useContext(AuthContext);

    const handleDialogOpen = (rowData) => {
        setEditData(rowData);
        setOpenDialog(true);
    };

    const handleDialogClose = () => {
        setOpenDialog(false);
    };

    const handleEditSubmit = async (event) => {
        event.preventDefault();
        setEditData({name: '', description: ''});
        setOpenDialog(false);
        try {
            const token = localStorage.getItem('token');
            const response = await axios.put(apiUrl+"/merchants", editData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            console.log("response: ", response);
            enqueueSnackbar('merchant was updated successful!', {variant: 'success'});
            refreshTable();
        } catch (error) {
            enqueueSnackbar('merchant updated error', {variant: 'error'});
            console.error("error during merchant update: ", error);
        }
    };

    const handleDelete = async (id) => {
        setOpenDialog(false);
        try {
            const token = localStorage.getItem('token');
            const response = await axios.delete(apiUrl+"/merchants/" + id, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            console.log("response: ", response);
            enqueueSnackbar('merchant was successful deleted', {variant: 'success'});
            refreshTable();
        } catch (error) {
            enqueueSnackbar(error.response.data.error, {variant: 'error'});
            console.error("error during merchant delete: ", error);
        }
    };

    const refreshTable = () => {
        setRefresh(prevState => !prevState);
    }

    useEffect(() => {
        const fetchMerchants = async () => {
            const token = localStorage.getItem('token');
            const response = await axios.get(apiUrl+"/merchants", {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            setLoggedIn(true);
            setMerchants(response.data);
            console.log("response: ", response);
        };
        fetchMerchants();
    }, [refresh]);

    const columns = [
        {field: 'uuid', headerName: 'ID', width: 330},
        {field: 'role', headerName: 'Role', width: 130},
        {field: 'name', headerName: 'Name', width: 130},
        {field: 'email', headerName: 'Email', width: 150},
        {field: 'status', headerName: 'Status', width: 80},
        {field: 'description', headerName: 'Description', width: 130},
        {
            field: 'actions',
            headerName: 'Actions',
            sortable: false,
            width: 155,
            disableClickEventBubbling: true,
            renderCell: (params) => {
                return (
                    <div>
                        <Button onClick={() => handleDialogOpen(params.row)}>Edit</Button>
                        <Button onClick={() => handleDelete(params.row.uuid)}>Delete</Button>
                    </div>
                );
            }
        },
    ];

    return (
        <div>
            <div style={{height: 600, width: '100%'}}>
                <DataGrid rows={merchants} columns={columns} pageSize={5} getRowId={(row) => row.uuid}/>
            </div>
            <Dialog open={openDialog} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Edit Merchant</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        name="name"
                        margin="dense"
                        id="name"
                        label="Name"
                        type="text"
                        fullWidth
                        value={editData.name}
                        onChange={(e) => setEditData({...editData, name: e.target.value})}
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={editData.status === "active"}
                                onChange={(event) => setEditData({
                                    ...editData,
                                    status: event.target.checked ? "active" : "inactive"
                                })}
                            />
                        }
                        label="Active Status"
                    />
                    <FormControl fullWidth margin="dense">
                        <InputLabel id="role-label">Role</InputLabel>
                        <Select
                            labelId="role-label"
                            id="role"
                            value={editData.role}
                            onChange={(e) => setEditData({...editData, role: e.target.value})}
                        >
                            <MenuItem value={"admin"}>Admin</MenuItem>
                            <MenuItem value={"merchant"}>Merchant</MenuItem>
                        </Select>
                    </FormControl>
                    <TextField
                        name="description"
                        margin="dense"
                        id="description"
                        label="Description"
                        type="text"
                        fullWidth
                        value={editData.description}
                        onChange={(e) => setEditData({...editData, description: e.target.value})}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleEditSubmit} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default Merchants;