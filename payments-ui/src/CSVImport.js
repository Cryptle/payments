import React, {useState} from 'react';
import axios from 'axios';
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import {enqueueSnackbar} from "notistack";
import {apiUrl} from "./App";


const CSVImport = () => {

    const [selectedFile, setSelectedFile] = useState(null);

    const handleFileSelect = (event) => {
        setSelectedFile(event.target.files[0]);
    };

    const handleUpload = () => {
        const token = localStorage.getItem('token');
        if (!selectedFile) return;

        let formData = new FormData();
        formData.append('file', selectedFile);

        axios.post(apiUrl + "/csv-import", formData, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'multipart/form-data'
            }
        })
            .then(res => {
                enqueueSnackbar('merchants CSV was uploaded successful!', {variant: 'success'});
            })
            .catch(err => {
                enqueueSnackbar('upload error', {variant: 'error'});
                console.log('upload error', err);
            });
    };

    return (
        <Container maxWidth="sm">
            <Box sx={{mt: 4}}>
                <Typography variant="h6">Upload CSV</Typography>
                <Box sx={{display: 'flex', justifyContent: 'space-between', mt: 2}}>
                    <input type="file" onChange={handleFileSelect} style={{display: 'none'}} id="upload-button"/>
                    <Button variant="contained" component="label" htmlFor="upload-button">
                        Merchants CSV
                    </Button>
                    <Button variant="contained" color="primary" onClick={handleUpload} disabled={!selectedFile}>
                        Upload
                    </Button>
                </Box>
            </Box>
        </Container>
    );
};

export default CSVImport;