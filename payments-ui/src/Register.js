import React, {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import {useSnackbar} from "notistack";
import {useNavigate} from "react-router-dom";
import {apiUrl} from "./App";

function Register() {

    const navigate = useNavigate();

    const [formData, setFormData] = useState({
        name: '',
        description: '',
        email: '',
        password: '',
        confirm_password: ''
    });

    const {enqueueSnackbar} = useSnackbar();

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (formData.password !== formData.confirm_password) {
            enqueueSnackbar('Passwords do not match!', {variant: 'error'});
            return;
        }
        try {
            console.log("handleSubmit: ", apiUrl + "/register");
            const response = await axios.post(apiUrl + "/register", formData);
            console.log("Registration response: ", response);
            enqueueSnackbar('Successfully registered!', {variant: 'success'});
            navigate('/login');
        } catch (error) {
            enqueueSnackbar('Registration failed!', {variant: 'error'});
            console.error("Error during registration: ", error);
        }
    };

    return (
        <Container maxWidth="sm">
            <Box sx={{mt: 8}}>
                <Typography variant="h4">Register</Typography>
                <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                    <TextField
                        name="name"
                        label="Name"
                        value={formData.name}
                        onChange={handleChange}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        name="email"
                        label="Email"
                        value={formData.email}
                        onChange={handleChange}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        name="password"
                        label="Password"
                        value={formData.password}
                        onChange={handleChange}
                        type="password"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        name="confirm_password"
                        label="Confirm Password"
                        value={formData.confirm_password}
                        onChange={handleChange}
                        type="password"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        fullWidth
                    >
                        Register
                    </Button>
                </form>
            </Box>
        </Container>
    );
}

export default Register;