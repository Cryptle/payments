import React, {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import axios from "axios";
import {enqueueSnackbar} from "notistack";
import {useNavigate} from "react-router-dom";
import {apiUrl, AuthContext} from "./App";


function Login() {

    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        email: '',
        password: '',
    });
    const {setLoggedIn} = React.useContext(AuthContext);
    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        });
    };

    const loginSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.post(apiUrl + "/login", formData);
            enqueueSnackbar('Successfully Login!', {variant: 'success'});
            const token = response.data.token;
            localStorage.setItem('token', token);
            setLoggedIn(true);
            navigate('/merchants');
        } catch (error) {
            enqueueSnackbar('Login failed!', {variant: 'error'});
            console.error("Error during login: ", error);
        }
    };

    return (
        <Container maxWidth="sm">
            <Box sx={{mt: 8}}>
                <Typography variant="h4">Login</Typography>
                <form noValidate autoComplete="off" onSubmit={loginSubmit}>
                    <TextField
                        name="email"
                        label="Email"
                        value={formData.email}
                        onChange={handleChange}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        name="password"
                        label="Password"
                        value={formData.password}
                        onChange={handleChange}
                        type="password"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        fullWidth
                    >
                        Login
                    </Button>
                </form>
            </Box>
        </Container>
    );
}

export default Login;
