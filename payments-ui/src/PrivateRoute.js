import { Navigate, useLocation } from "react-router-dom";

function PrivateRoute({ children }) {
    let token = localStorage.getItem("token");
    let location = useLocation();
    return (
        token ? children : <Navigate to="/login" state={{ from: location }} />
    );
}
export default PrivateRoute;